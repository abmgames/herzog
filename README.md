#Notice

In order to run LearnNet you should use the following libraries :

- PList parser - which can be found in ABMGamesLTD's BitBucket repositories
- Android-support-v7-appcompat - an Android support library. can be found in the Android's SDK folder under at ".../extras/android/support/v7"
- Google play services libs - Google Play's official services libraries. can be found in the Android's SDK folder at ".../extras/google/google_play_services"