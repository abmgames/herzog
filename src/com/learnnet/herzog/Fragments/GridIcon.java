package com.learnnet.herzog.Fragments;

public class GridIcon {

    private int iconId;
    private int colorId;
    private int textId;

    public GridIcon(int iconId, int colorId, int textId)
    {
        this.iconId  = iconId;
        this.colorId = colorId;
        this.textId  = textId;
    }

    public int getIconId()
    {
        return this.iconId;
    }

    public int getColorId()
    {
        return this.colorId;
    }

    public int getTextId()
    {
        return this.textId;
    }
}
