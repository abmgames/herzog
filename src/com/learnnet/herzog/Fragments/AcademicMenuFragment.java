package com.learnnet.herzog.Fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.learnnet.herzog.MyApplication;
import com.learnnet.herzog.R;
import com.learnnet.herzog.Adapters.MyAcademicMenuListViewAdapter;
import com.learnnet.herzog.Communication.MyMichlolRequests;
import com.learnnet.herzog.Extensions.MyAcademicMenuManager;
import com.learnnet.herzog.Interface.MyInterface;
import com.learnnet.herzog.Utils.GlobalDefs;
import com.learnnet.herzog.Utils.xmlParser;
import com.learnnet.herzog.activity.Exams;
import com.learnnet.herzog.activity.Grades;
import com.learnnet.herzog.activity.MainMenu;
import com.learnnet.herzog.activity.Messages;
import com.learnnet.herzog.activity.OnlineEducation;
import com.learnnet.herzog.activity.Requests;
import com.learnnet.herzog.activity.Schedule;
import com.learnnet.herzog.activity.Survey;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class AcademicMenuFragment extends Fragment implements GlobalDefs, OnItemClickListener, MyInterface {

	// =================================================
	// FIELDS
	// =================================================

	private ArrayList<Map<String, String>> studentTableFeaturesArray;
	private String average = "";
	private MyApplication mApp;

	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	//Global data
    private int screenWidth, screenHeight;
    private int gridHeight, gridWidth;

    private final int ROWS_IN_SCREEN = 5;
    private final int COLUMNS_IN_ROW = 3;

    private float rowHeight, rowWidth;
    private float rowSpace;

    private String packageName;

    //Elements
    private LinearLayout gridContainer;

    private enum ROW_TYPE{
        ONLY_COLUMNS, LEFT_IMAGE, RIGHT_IMAGE, RIGHT_BIG_IMAGE, LINE_IMGAGE_ROW
    };

    private enum COLUMN_TYPE{
        TESTS, GRADES, ONLINE_TEACHING, MESSAGES, SCHEDULE, POLL, AVG, REQUESTS
    }

    private ROW_TYPE[] rowsType = {
            ROW_TYPE.RIGHT_IMAGE,
            ROW_TYPE.LEFT_IMAGE,
            ROW_TYPE.ONLY_COLUMNS,
            ROW_TYPE.RIGHT_BIG_IMAGE,
            ROW_TYPE.LINE_IMGAGE_ROW
    };

    private COLUMN_TYPE[] columnTypes = {
            COLUMN_TYPE.TESTS, COLUMN_TYPE.GRADES, COLUMN_TYPE.ONLINE_TEACHING, COLUMN_TYPE.MESSAGES, COLUMN_TYPE.SCHEDULE, COLUMN_TYPE.POLL, COLUMN_TYPE.AVG, COLUMN_TYPE.REQUESTS
    };

    private int[] rowImage = {R.drawable.pitcher_image, R.drawable.class_image, 0, R.drawable.pepoles_image};

    private int rowPosition    = 0;
    private int columnPosition = 0;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_academic_menu, container, false);

		mApp = ((MainMenu)getActivity()).getMyApp();

		/*MyAcademicMenuManager mamm = new MyAcademicMenuManager(getPropertiesInt(PROP_BRANCH_ID), ((MainMenu)getActivity()).getIsTeacher());
		studentTableFeaturesArray = mamm.getMenu();

		ArrayList<Map<String, String>> data = new ArrayList<Map<String,String>>(studentTableFeaturesArray);

		ListView mAcademicList = (ListView) v.findViewById(R.id.lv_academic_menu);
		mAcademicList.setAdapter(new MyAcademicMenuListViewAdapter(((MainMenu)getActivity()), data));
		mAcademicList.setOnItemClickListener(this);*/

		this.screenHeight = Screen.height(getActivity());
        this.screenWidth  = Screen.width(getActivity());

        this.gridHeight = (int)(screenHeight * 0.96);
        this.gridWidth  = (int)(screenWidth * 0.96);
        this.rowSpace   = (gridHeight * 0.01f);

        this.rowHeight = (((gridHeight - (rowSpace * (ROWS_IN_SCREEN - 1))) / ROWS_IN_SCREEN) - rowSpace);

        this.packageName = getActivity().getPackageName();

        //Elements
        this.gridContainer = (LinearLayout) v.findViewById(R.id.grid_container);
        Log.i("myapp", "Gridcontainer: " + gridContainer.toString());
        gridContainer.setBackgroundColor(Color.parseColor("#15868e"));
        
        int totalRows = rowsType.length;

        for(; rowPosition < totalRows; rowPosition++)
        {
            View row = buildRow(rowsType[rowPosition]);

            if(row != null)
            {
                if(rowPosition > 0)
                {
                    LayoutInflater ift = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View gridSpaceRow = ift.inflate(R.layout.grid_space, gridContainer, false);

                    gridSpaceRow.getLayoutParams().height = (int)rowSpace;
                    gridContainer.addView(gridSpaceRow);
                }

                try{
                	row.getLayoutParams().height = (int)rowHeight;
                    gridContainer.addView(row);
                }
                catch(Exception e){
                	e.printStackTrace();
                }
            }
        }
		
		return v;
	}
	
	private View buildRow(ROW_TYPE type)
    {
        if(type == ROW_TYPE.ONLY_COLUMNS)
            return buildRegularRow();
        else if(type == ROW_TYPE.LEFT_IMAGE || type == ROW_TYPE.RIGHT_IMAGE)
            return buildColumnImageView(type);
        else if(type == ROW_TYPE.RIGHT_BIG_IMAGE)
            return buildBigImageRow();
        else if(type == ROW_TYPE.LINE_IMGAGE_ROW)
        {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View row = inflater.inflate(R.layout.grid_row_image, gridContainer, false);
            ((ImageView) row.findViewById(R.id.row_image)).setImageResource(R.drawable.herzog_line);

            return row;
        }

        return null;
    }

    private View buildRegularRow()
    {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.grid_row, gridContainer, false);

        int columnsInThisRow = (columnPosition + COLUMNS_IN_ROW);
        int columnCounter    = 0;

        updateRowColumns(row, 0, COLUMNS_IN_ROW);

        return row;
    }

    private View buildColumnImageView(ROW_TYPE type)
    {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.grid_row, gridContainer, false);

        ImageView columnImage = null;

        try{
            columnImage = new ImageView(getActivity());

            if(rowImage[rowPosition] > 0)
            {
                columnImage.setImageResource(rowImage[rowPosition]);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                );

                columnImage.setScaleType(ImageView.ScaleType.FIT_XY);
                columnImage.setLayoutParams(params);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if(type == ROW_TYPE.LEFT_IMAGE)
        {
            if(columnImage != null)
                ((FrameLayout) row.findViewById(R.id.column_0)).addView(columnImage);

            updateRowColumns(row, 1, 2);
        }
        else
        {
            if(columnImage != null)
                ((FrameLayout) row.findViewById(R.id.column_2)).addView(columnImage);

            updateRowColumns(row, 0, 2);
        }

        return row;
    }

    private View buildBigImageRow()
    {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_big_right_image, gridContainer, false);

        updateRowColumns(row, 0, 1);

        return row;
    }

    private void updateRowColumns(View row, int startColumn, int totalColumn)
    {
        int columnsInThisRow = (columnPosition + totalColumn);
        int columnCounter    = startColumn;

        for(; columnPosition < columnsInThisRow; columnPosition++)
        {
            try{
                int columnId = row.getResources().getIdentifier("column_" + columnCounter, "id", packageName);

                if(columnId > 0)
                {
                    try{
                        buildColumn(columnTypes[columnPosition], (FrameLayout) row.findViewById(columnId));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }

                columnCounter++;
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void buildColumn(COLUMN_TYPE type, FrameLayout column) throws Exception
    {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Build column content
        final GridIcon gridIcon = getIconByType(type);

        View columnContent = inflater.inflate(R.layout.row_column, gridContainer, false);
        ((ImageView) columnContent.findViewById(R.id.column_icon)).setImageResource(gridIcon.getIconId());
        ((TextView) columnContent.findViewById(R.id.column_text)).setText(getString(gridIcon.getTextId()));

        columnContent.findViewById(R.id.column_container).setBackgroundResource(gridIcon.getColorId());
        
        columnContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(gridIcon.getIconId());
            }
        });

        column.addView(columnContent);
    }

    private GridIcon getIconByType(COLUMN_TYPE type)
    {
        if(type == COLUMN_TYPE.AVG)
            return new GridIcon(R.drawable.avg_icon, R.color.avg_icon, R.string.avg_icon);
        else if(type == COLUMN_TYPE.TESTS)
            return new GridIcon(R.drawable.tests_icon, R.color.tests_icon, R.string.tests_icon);
        else if(type == COLUMN_TYPE.GRADES)
            return new GridIcon(R.drawable.grades_icon, R.color.grades_icon, R.string.grades_icon);
        else if(type == COLUMN_TYPE.ONLINE_TEACHING)
            return new GridIcon(R.drawable.teaching_online_icon, R.color.teaching_online_icon, R.string.teaching_online_icon);
        else if(type == COLUMN_TYPE.MESSAGES)
            return new GridIcon(R.drawable.messages_icon, R.color.messages_icon, R.string.messages_icon);
        else if(type == COLUMN_TYPE.SCHEDULE)
            return new GridIcon(R.drawable.schedule_icon, R.color.schedule_icon, R.string.schedule_icon);
        else if(type == COLUMN_TYPE.POLL)
            return new GridIcon(R.drawable.poll_icon, R.color.poll_icon, R.string.poll_icon);
        else if(type == COLUMN_TYPE.REQUESTS)
            return new GridIcon(R.drawable.requests_icon, R.color.requests_icon, R.string.requests_icon);

        return new GridIcon(R.drawable.default_icon, R.color.default_icon, R.string.default_icon);
    }

    private void onItemClick(int itemId)
    {
        Intent i;
        switch (itemId) { // arg2 stands for list position

            case R.drawable.messages_icon:
                // Messages
                mApp.sendFlurryEvent(FLURRY_MESSAGES);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), Messages.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.grades_icon:
                // Grades
                mApp.sendFlurryEvent(FLURRY_GRADES);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), Grades.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.tests_icon:
                mApp.sendFlurryEvent(FLURRY_EXAMS);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), Exams.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.schedule_icon:
                // Schedule
                mApp.sendFlurryEvent(FLURRY_SCHEDULE);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), Schedule.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.avg_icon:
                // Average grade
                mApp.sendFlurryEvent(FLURRY_AVERAGE);
                if (MyApplication.isInternetAvailable) {
                    showAverageGrade();
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.requests_icon:
                // Student requests
                mApp.sendFlurryEvent(FLURRY_REQUESTS);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), Requests.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.teaching_online_icon:
                // Online education
                mApp.sendFlurryEvent(FLURRY_ONLINE_EDUCATION);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), OnlineEducation.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            case R.drawable.poll_icon:
                // Education survey
                mApp.sendFlurryEvent(FLURRY_SURVEY);
                if (MyApplication.isInternetAvailable) {
                    i = new Intent((MainMenu)getActivity(), Survey.class);
                    startActivity(i);
                }
                else {
                    mApp.showNoInternetDialog((MainMenu)getActivity());
                }
                break;
            default:
                break;
        }
    }
    
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent i;
		switch (arg2) { // arg2 stands for list position
		case 0:
			// Messages
			mApp.sendFlurryEvent(FLURRY_MESSAGES);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Messages.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 1:
			// Grades
			mApp.sendFlurryEvent(FLURRY_GRADES);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Grades.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 2:
			// Exams
			mApp.sendFlurryEvent(FLURRY_EXAMS);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Exams.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 3:
			// Schedule
			mApp.sendFlurryEvent(FLURRY_SCHEDULE);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Schedule.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 4:
			// Average grade
			mApp.sendFlurryEvent(FLURRY_AVERAGE);
			if (MyApplication.isInternetAvailable) {
				showAverageGrade();
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 5:
			// Student requests
			mApp.sendFlurryEvent(FLURRY_REQUESTS);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Requests.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 6:
			// Online education
			mApp.sendFlurryEvent(FLURRY_ONLINE_EDUCATION);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), OnlineEducation.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 7:
			// Education survey
			mApp.sendFlurryEvent(FLURRY_SURVEY);
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Survey.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		default:
			break;
		}
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		MyApplication mApp = ((MainMenu)getActivity()).getMyApp();
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			Map<String, Object> map = new HashMap<String, Object>();
			map = (Map<String, Object>) mApp.mPropertiesSpecific.get(key);
			return map;
		}
		else {
			return null;
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void showAverageGrade() {
		new GetAverageTask().execute();
	}

	private String michlolRequestAverage(String username, String url) {
		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
		return michlolMessagesRequest.call(REQ_AVERAGE, username, "UnsedValueJustForChecks",
				null, null, url, false); // false doesn't matter here.
	}

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================

	private class GetAverageTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((MainMenu)getActivity()).displaySpinner();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			StringBuilder sb = new StringBuilder();

			String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
			String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

			sb.append(michlolRequestAverage(username, url));

			xmlParser xp = new xmlParser();
			average = xp.parseAverage(sb.toString());

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			((MainMenu)getActivity()).hideSpinner();
			((MainMenu)getActivity()).displayAverageDialog(average);
		}
	}

}