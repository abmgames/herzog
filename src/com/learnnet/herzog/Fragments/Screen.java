package com.learnnet.herzog.Fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.view.Display;
import android.view.WindowManager;

public class Screen {

    private Context ctx;

    public Screen(Context ctx) {
        this.ctx = ctx;
    }

    public int brightnessLevel() {
        return Settings.System.getInt(this.ctx.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, -1);
    }

    public int timeOut() {
        return Settings.System.getInt(this.ctx.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, -1);
    }

    public boolean autoMode() {
        if (Settings.System.getInt(this.ctx.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, -1) > 0)
            return true;

        return false;
    }

    public static int width(Activity ctx)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            return newSdkVersionWidth(ctx);
        else
            return oldSdkVersionWidth(ctx);
    }

    public static int height(Activity ctx)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            return newSdkVersionHeight(ctx);
        else
            return oldSdkVersionHeight(ctx);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static int newSdkVersionWidth(Activity ctx)
    {
        try{
            WindowManager w = ctx.getWindowManager();

            Point size = new Point();
            w.getDefaultDisplay().getSize(size);

            return size.x;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static int newSdkVersionHeight(Activity ctx)
    {
        try{
            WindowManager w = ctx.getWindowManager();

            Point size = new Point();
            w.getDefaultDisplay().getSize(size);

            return size.y;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }

    @Deprecated
    public static int oldSdkVersionWidth(Activity ctx)
    {
        try{
            WindowManager w = ctx.getWindowManager();

            Display d = w.getDefaultDisplay();
            return d.getWidth();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }

    @Deprecated
    public static int oldSdkVersionHeight(Activity ctx)
    {
        try{
            WindowManager w = ctx.getWindowManager();

            Display d = w.getDefaultDisplay();
            return d.getHeight();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }


}