package com.learnnet.herzog.richPush;

import java.text.SimpleDateFormat;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.TextView;

import com.learnnet.herzog.R;
import com.learnnet.herzog.richPush.RichPushMessageAdapter.ViewBinder;
import com.urbanairship.richpush.RichPushMessage;

public class RichPushSampleInboxFragment extends InboxFragment {

    private static final SimpleDateFormat UA_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public int getRowLayoutId() {
        return R.layout.inbox_message;
    }

    @Override
    public int getEmptyListStringId() {
        return R.string.my_no_messages;
    }

    @Override
    protected ViewBinder createMessageBinder() {
        return new RichPushMessageAdapter.ViewBinder() {

            @Override
            public void bindView(View view, final RichPushMessage message) {
//                View unreadIndicator = view.findViewById(R.id.unread_indicator);
                TextView title = (TextView) view.findViewById(R.id.title);
                TextView timeStamp = (TextView) view.findViewById(R.id.date_sent);
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.message_checkbox);

                if (message.isRead()) {
//                    unreadIndicator.setBackgroundColor(Color.BLACK);
//                    unreadIndicator.setContentDescription("Message is read");
                	view.setBackgroundColor(getResources().getColor(R.color.my_light_gray));
                } else {
//                    unreadIndicator.setBackgroundColor(Color.YELLOW);
//                    unreadIndicator.setContentDescription("Message is unread");
                	view.setBackgroundColor(getResources().getColor(R.color.my_white));
                }

                title.setText(message.getTitle());
                timeStamp.setText(UA_DATE_FORMATTER.format(message.getSentDate()));

                checkBox.setChecked(isMessageSelected(message.getMessageId()));

                checkBox.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onMessageSelected(message.getMessageId(), checkBox.isChecked());
                    }
                });
                view.setFocusable(false);
                view.setFocusableInTouchMode(false);
            }
        };
    }
}