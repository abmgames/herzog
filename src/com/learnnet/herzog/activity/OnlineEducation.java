package com.learnnet.herzog.activity;

import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.learnnet.herzog.MyApplication;
import com.learnnet.herzog.R;
import com.learnnet.herzog.Extensions.MyInstituteUrlManager;
import com.learnnet.herzog.Interface.MyInterface;
import com.learnnet.herzog.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       2014-05-09
 */
public class OnlineEducation extends Activity implements GlobalDefs, MyInterface {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private WebView webView;
	private ProgressBar mProgressBar;
	private MyApplication mApp;
	private String javaScriptQuery;
	private boolean isFirstPage = true;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.web_view_layout);
		
		mApp = (MyApplication)getApplication();
		
		CookieSyncManager.createInstance(this); 
	    CookieManager cookieManager = CookieManager.getInstance();
	    cookieManager.removeAllCookie();
		
		String userName = mApp.mPrefs.getString(PREF_LOGIN_USER_NAME, "");
		String password = mApp.mPrefs.getString(PREF_LOGIN_PASSWORD, "");
		javaScriptQuery = "javascript:document.getElementById('username').value='" + userName + "';"
				+ "javascript:document.getElementById('password').value='" + password + "';"
				+ "javascript:document.getElementById('loginbtn').click();";
		 
		final MyInstituteUrlManager mium = new MyInstituteUrlManager(getPropertiesInt(PROP_BRANCH_ID));
		mProgressBar = (ProgressBar) findViewById(R.id.pbWebSpinner);
		this.webView = (WebView) findViewById(R.id.webview);
		
		this.webView.setWebViewClient(new MyWebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				mProgressBar.setVisibility(View.INVISIBLE);
				if (isFirstPage) {
					isFirstPage = false;
					webView.loadUrl(javaScriptQuery);
				}
				else {
					view.setVisibility(View.VISIBLE);
				}
			}
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				mProgressBar.setVisibility(View.VISIBLE);
			}
		});
		
		this.webView.getSettings().setBuiltInZoomControls(true); 
		this.webView.getSettings().setSupportZoom(true);
		this.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);   
		this.webView.getSettings().setAllowFileAccess(true); 
		this.webView.getSettings().setDomStorageEnabled(true);
		this.webView.getSettings().setJavaScriptEnabled(true);
		this.webView.getSettings().setLoadWithOverviewMode(true);
		this.webView.getSettings().setUseWideViewPort(true);
		this.webView.loadUrl(mium.onlineEducationUrl);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, FLURRY_K);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	
	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	public class MyWebViewClient extends WebViewClient {        
		/* (non-Java doc)
		 * @see android.webkit.WebViewClient#shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String)
		 */

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.endsWith(".mp4")) 
			{
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse(url), "video/*");

				view.getContext().startActivity(intent);
				return true;
			}
			else if(!url.contains("docs.google.com/gview?embedded") &&
					(url.endsWith(".pdf") || url.endsWith(".docx") || url.endsWith(".dox") || url.endsWith(".ppt")
							|| url.endsWith(".pptx") || url.endsWith(".xls") || url.endsWith(".xlsx"))){
				webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);
				return true;
			}
			else {
				return super.shouldOverrideUrlLoading(view, url);
			}
		}
	}
	
}
