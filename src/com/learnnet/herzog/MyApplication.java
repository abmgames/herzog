package com.learnnet.herzog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.learnnet.herzog.Interface.MyInterface;
import com.learnnet.herzog.Objects.IdmLoginData;
import com.learnnet.herzog.Objects.UserDetailsData;
import com.learnnet.herzog.Objects.YearsData;
import com.learnnet.herzog.Utils.GlobalDefs;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.groboot.pushapps.PushAppsRegistrationInterface;
import com.groboot.pushapps.PushManager;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MyApplication extends Application implements GlobalDefs, MyInterface {

	// =================================================
	// FIELDS
	// =================================================
	
	public Map<String, Map<String, Object>> mProperties;
	public Map<String, Object> mPropertiesSpecific;
	public SharedPreferences mPrefs;
	public UserDetailsData mCurrentUserDetails;
	public List<YearsData> mCurrentUserYears;
	public IdmLoginData mIdmLoginData;
	public static boolean isInternetAvailable;
	public HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	//API
	public static String[] Kxx;
	public static String TOKEN;
	public static String pU, pP;

	//Debug - Do not forget to disable debug mode before you publish a new version!!
	public static final boolean DEBUG_MODE = false;

	// Rich Push
	public static final String MESSAGE_ID_RECEIVED_KEY = "com.urbanairship.richpush.sample.MESSAGE_ID_RECEIVED";
    public static final String HOME_ACTIVITY = "Home";
    public static final String INBOX_ACTIVITY = "Inbox";
    public static final String[] navList = new String[] {
        HOME_ACTIVITY, INBOX_ACTIVITY
    };
	
    public static final String GOOGLE_API_PROJECT_NUMBER = "330600762724"; //your sender id (google API project id)
    public static final String PUSHAPPS_APP_TOKEN = "71fe4ec1-a64f-49b7-a311-a8174b08c3d2"; //your application token from PushApps
    
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		PushManager mPushManager = PushManager.getInstance(getApplicationContext());
        mPushManager.registerForRegistrationEvents(new PushAppsRegistrationInterface() {
 
        	@Override
            public void onUnregistered(Context context, String paramString) {
                // The device has been unregistered
            	Log.i("myapp", "onUnregistered paramString: " +  paramString);
            }
 
            @Override
            public void onRegistered(Context context, String registrationId) {
                //The device is registered to the GCM service, the registrationId is the unique device token which will be used for sending messages.
            	Log.i("myapp", "onRegistered registrationId: " +  registrationId);
            }

			@Override
			public void onError(Context arg0, String arg1) {
				// TODO Auto-generated method stub
				Log.i("myapp", "onError arg1: " +  arg1);
			}
        });
        PushManager.init(getApplicationContext(), GOOGLE_API_PROJECT_NUMBER, PUSHAPPS_APP_TOKEN);
        		
		mPrefs = getSharedPreferences(PREF_FILE_NAME, Activity.MODE_PRIVATE);
		
		getTracker(TrackerName.APP_TRACKER);
				
		mPrefs = getSharedPreferences(PREF_FILE_NAME, Activity.MODE_PRIVATE);
	}

	@Override
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			// There are no active networks.
			return false;
		} else
			return true;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	// =================================================
		// STATIC VARIABLES AND METHODS
		// =================================================

		// The following line should be changed to include the correct property id.
		private static final String PROPERTY_ID = "UA-52666410-8";

		public enum TrackerName {
			APP_TRACKER, // Tracker used only in this app.
			GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
		}

		// =================================================
		// CLASS LOGIC
		// =================================================

		public synchronized Tracker getTracker(TrackerName trackerId) {
			if (!mTrackers.containsKey(trackerId)) {

				GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
				Tracker t;

				if (trackerId == TrackerName.APP_TRACKER) {
					t = analytics.newTracker(R.xml.app_tracker);
				}
				else {
					t = analytics.newTracker(PROPERTY_ID);
				}
				mTrackers.put(trackerId, t);
			}
			return mTrackers.get(trackerId);
		}

		public void sendAnalyticScreen(String screenName) {

			Tracker t = getTracker(TrackerName.APP_TRACKER);
			t.setScreenName(screenName);
			t.send(new HitBuilders.AppViewBuilder().build());
		}

	
	// =================================================
	// CLASS LOGIC
	// =================================================

	public void sendFlurryEvent(String event) {
		FlurryAgent.logEvent(event);
	}
	
	public void showNoInternetDialog(Context context) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		alertDialog.setTitle(getString(R.string.my_error_title_no_internet));

		alertDialog.setMessage(getString(R.string.splash_no_internet));

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
				getString(R.string.my_ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		try {
			alertDialog.show();
		} catch (Exception e) {
			// handle the exception
		}
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	public static class ConnectionChangeReceiver extends BroadcastReceiver
	{
	  @Override
	  public void onReceive( Context context, Intent intent )
	  {
	    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
	    NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
//	    NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(     ConnectivityManager.TYPE_MOBILE );
	    if ( activeNetInfo != null ) {
	    	isInternetAvailable = true;
	    }
	    else {
	    	isInternetAvailable = false;
	    }
//	    if( mobNetInfo != null )
//	    {
//	      Toast.makeText( context, "Mobile Network Type : " + mobNetInfo.getTypeName(), Toast.LENGTH_SHORT ).show();
//	    }
	  }
	}

	public static boolean pKeyParse(String pKey){
		if(TextUtils.isEmpty(pKey))
			return false;

		try{
			Kxx = pKey.split("xx\\$\\$yy");
		}
		catch (Exception e){
			e.printStackTrace();
		}

		return pKeyIsAvailable();
	}

	public static boolean pKeyIsAvailable(){
		if(Kxx == null || Kxx.length < 2)
			return false;

		return true;
	}
	
}
